<?php

namespace Database\Seeders;

use App\Models\Vaccine;
use Illuminate\Database\Seeder;

class VaccineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Vaccine::create([
            'name' => 'Pfizer'
        ]);
        Vaccine::create([
            'name' => 'AstraZeneca'
        ]);
        Vaccine::create([
            'name' => 'J&J'
        ]);
        Vaccine::create([
            'name' => 'Moderna'
        ]);
        Vaccine::create([
            'name' => 'Novax'
        ]);
    }
}
