<?php

namespace Database\Seeders;

use App\Models\Eps;
use Illuminate\Database\Seeder;

class EpsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eps::create([
            'name' => 'Sos'
        ]);
        Eps::create([
            'name' => 'Coomeva'
        ]);
        Eps::create([
            'name' => 'Sura'
        ]);
        Eps::create([
            'name' => 'Nueva eps'
        ]);
    }
}
