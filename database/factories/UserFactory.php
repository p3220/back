<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'cc' => $this->faker->name(),
            'name' => $this->faker->name(),
            'lastname' => $this->faker->lastName(),
            'age' => $this->faker->randomNumber(),
            'eps_id' => 1,
        ];
    }
}
