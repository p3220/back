<?php

namespace App\GraphQL\Mutations;

use App\Models\Vaccine;
use Illuminate\Support\Facades\Log;

class Vaccinated
{
    /**
     * @param null $_
     * @param array<string, mixed> $args
     */
    public function __invoke($_, array $args)
    {
        $vaccinated = \App\Models\Vaccinated::create($args);

        return $vaccinated;
    }
}
